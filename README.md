# Earthdata Migrate Source Module

## Contents of this File

* Introduction
* Installation
* Maintainers

### Introduction

The Earthdata Migrate Source module prepares data to be migrated from Conduit to Drupal 9.

### Installation

* Install as you would normally install a contributed Drupal module.
   See: [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

### Maintainers/Support

* [SSAI](https://www.ssaihq.com/)
* Goran Halusa: [goran.halusa@nasa.gov](goran.halusa@nasa.gov)