<?php

/**
 * @file
 * Contains \Drupal\earthdata_migrate_source\Controller\EarthdataMigrateSourceController.
 */

namespace Drupal\earthdata_migrate_source\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Utility\UrlHelper;

/**
 * Controller routines for book routes.
 */
class EarthdataMigrateSourceController extends ControllerBase {

  /**
   * The Account interface.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * EAD Processor
   *
   * @var \Drupal\earthdata_migrate_source\Processors\Processor
   */
  private $processor;

  /**
   * Constructs an EarthdataMigrateSourceController object.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The Account interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(AccountInterface $current_user, MessengerInterface $messenger) {
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
    $this->processor = \Drupal::service('earthdata_migrate_source.processor');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('messenger')
    );
  }

  /**
   * Dispatch Request
   *
   * @return array
   *   A Drupal render array.
   */
  public function dispatchRequest() {

    $data = $settings = [];
    $message = $error = '';
    $response = new Response();
    $request = \Drupal::request();
    // e.g. ?foo=no&bar=yes
    // http://earthdata/process-page?foo=no&bar=yes
    $post = $request->getContent();
    // Full path: /foo/bar
    $path_info = $request->getpathInfo();
    $arg = explode('/', $path_info);
    unset($arg[0]);
    $args = array_values($arg);
    // Query string: bar=yes&foo=no
    $query_string = $request->getQueryString();
    // URL parameter array: ["foo" => "no", "bar" => "yes"]
    $parameter_bag = $request->query;
    $url_parameters = $parameter_bag->all();

    // Use the dump() and dd() functions output variables.
    // dump() will output the variable and continue.
    // dd() will output the variable and halt.

    // dump($path_info);
    // dump($args);
    // dump($query_string);
    // dd($url_parameters);
    // dd($post);

    // [GET] Process page
    if (in_array('process-page', $args)) {
      $data = $this->processor->doStuff(['foo' => 'bar', 'title' => 'So Say We All'], 'Battlestar Galactica');
      return $data;
    }

    // Return an empty $data array.
    return $data;
  }

  /**
   * Test Page
   *
   * @return array
   *   A Drupal render array.
   */
  public function testPage() {

    $data = [];
    $message = $error = '';

    // $message = 'Foo';
    // $error = 'Bar';

    $this->messenger->addStatus('Hello ' . $this->currentUser->getAccountName() . '!');

    // Error handling.
    if (!empty($error)) {
      // Log to the database. To view logs, go to admin/reports/dblog
      \Drupal::logger('earthdata_migrate_source')->error($error);
      // Set the message for the front-end.
      $this->messenger->addError('Error(s): ' . $error);
    }

    // Messages.
    if (!empty($message)) {
      // Log to the database. To view logs, go to admin/reports/dblog
      \Drupal::logger('earthdata_migrate_source')->info($message);
      // Set the message for the front-end.
      $this->messenger->addWarning('Status: ' . $message);
    }

    if (empty($error) && empty($message)) {
      // Set the message for the front-end.
      $this->messenger->addWarning('This page does absolutely nothing.');
      $this->messenger->addError('But here\'s an example of an error!');
    }

    $data = [
      'greeting' => 'Hello ' . $this->currentUser->getAccountName() . '!',
      'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
        tempor incididunt ut labore et dolore magna aliqua.',
    ];

    return [
      '#theme' => 'earthdata_migrate_source_test_page',
      '#data' => $data,
      '#message' => $message,
      '#error' => $error,
      // '#attached' => [
      //   'library' => [
      //     'earthdata_migrate_source/earthdata_library',
      //   ],
      // ],
    ];
  }

}
