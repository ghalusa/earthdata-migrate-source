<?php

/**
 * @file
 * Contains \Drupal\earthdata_migrate_source\Processors\Processor.
 */

namespace Drupal\earthdata_migrate_source\Processors;

use Symfony\Component\HttpFoundation\RequestStack;

class Processor implements ProcessorInterface {

  /**
   * Configurations for the earthdata_migrate_source module.
   */
  private $config;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Constructor
   */
  public function __construct(RequestStack $request_stack)
  {
    $this->config = \Drupal::config('earthdata_migrate_source.settings');
    $this->request = $request_stack->getCurrentRequest();
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
    );
  }

  /**
   * Do Stuff
   *
   * This method does stuff.
   *
   * @param array $some_array
   * @param string $some_string
   * @return array
   */
  public function doStuff($some_array = [], $some_string = '') {

    // URL parameter array: ["foo" => "no", "bar" => "yes"]
    // http://earthdata/process-page?foo=no&bar=yes
    $parameter_bag = $this->request->query;
    $get = $parameter_bag->all();

    $data = [];
    $title = !empty($some_array['title']) ? $some_array['title'] : '';
    $prefix = !empty($some_string) ? $some_string . ' - ' : '';

    $data = [
      '#theme' => 'earthdata_migrate_source_process_page',
      '#full_title' => $prefix . ucwords($title),
      '#settings' => [$this->config->get('foo_setting'), $this->config->get('bar_setting')],
      '#url_parameters' => $get,
    ];

    return $data;
  }

}
