<?php

/**
 * @file
 * Contains \Drupal\earthdata_migrate_source\Processors\ProcessorInterface.
 */

namespace Drupal\earthdata_migrate_source\Processors;

interface ProcessorInterface {

  /**
   * Do Stuff
   * @return array
   *
   */
  public function doStuff(array $some_array, string $some_string);

}
